package mx.unitec.moviles.practica6.repository

import android.app.Application
import androidx.lifecycle.LiveData
import mx.unitec.moviles.practica6.dao.ContactDao
import mx.unitec.moviles.practica6.db.ContactDatabase
import mx.unitec.moviles.practica6.model.Contact

class ContactsRepository(application: Application) {
    private val contactDao: ContactDao? = ContactDatabase.getInstance(application)?.contactDao()

    suspend fun insert(contact: Contact) {
        contactDao!!.insert(contact)
    }

    fun getContacts(): LiveData<List<Contact>> {
        return contactDao!!.getOrderedAgenda()
    }
}